﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class StopDraggingMyWindowAround : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
{

    [SerializeField]
    private RectTransform myWindow;
    
    private Canvas canvas;
    private RectTransform parentRect;

    public void Awake()
    {
        canvas = transform.GetComponentInParent<Canvas>();
        parentRect = transform.GetComponentInParent<RectTransform>();
    }

    public void OnDrag(PointerEventData eventData)
    {
        //Debug.Log("Dragging " + gameObject.name);
        myWindow.anchoredPosition += eventData.delta / canvas.scaleFactor;

        // Can drag off top or left of screen
        if( myWindow.anchoredPosition.y > 0.0f)
        {
            myWindow.anchoredPosition = new Vector2(myWindow.anchoredPosition.x, 0.0f);
        }
        if (myWindow.anchoredPosition.x < 0.0f)
        {
            myWindow.anchoredPosition = new Vector2(0.0f, myWindow.anchoredPosition.y);
        }

        // Can't drag off right or bottom of screen
        if (myWindow.anchoredPosition.y * canvas.scaleFactor - 55f < -Screen.height) 
        {
            myWindow.anchoredPosition = new Vector2(myWindow.anchoredPosition.x, -Screen.height/canvas.scaleFactor + 55f);
        }
        if (myWindow.anchoredPosition.x * canvas.scaleFactor + 150f > Screen.width)
        {
            myWindow.anchoredPosition = new Vector2(Screen.width/canvas.scaleFactor -150f, myWindow.anchoredPosition.y);
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {

    }

    public void OnEndDrag(PointerEventData eventData)
    {

    }
}
