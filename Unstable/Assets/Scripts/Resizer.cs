﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Resizer : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    [SerializeField]
    private RectTransform myWindow;

    private Canvas canvas;
    private Vector2 initialMousePos;
    private Vector2 initialDeltaSize;

    public void Awake()
    {
        canvas = transform.GetComponentInParent<Canvas>();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        initialMousePos = Input.mousePosition;
        initialDeltaSize = myWindow.sizeDelta * canvas.scaleFactor;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector2 temp = (Vector2)initialMousePos - (Vector2)Input.mousePosition;
        temp = new Vector2(-temp.x, temp.y);
        temp += initialDeltaSize;
        myWindow.sizeDelta = temp / canvas.scaleFactor;
    }

}
