﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class Loader : MonoBehaviour
{

    public VideoPlayer videoPlayer;
    public Transform mainPanel;

    private AsyncOperation asyncLoad;

    // Start is called before the first frame update
    void Start()
    {
        videoPlayer.url = System.IO.Path.Combine(Application.streamingAssetsPath, "intro.mov");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartVideo()
    {
        mainPanel.gameObject.SetActive(false);
        videoPlayer.Play();
        // Use a coroutine to load main in the background
        StartCoroutine(LoadAsyncScene());
        videoPlayer.loopPointReached += JumpScene;
    }

    IEnumerator LoadAsyncScene()
    {
        Debug.Log("Starting coroutine ...");

        asyncLoad = SceneManager.LoadSceneAsync("Main");
        // Don't show the scene when done loading.
        asyncLoad.allowSceneActivation = false;
        // yield to other processes until the scene is loaded
        while (!asyncLoad.isDone)
        {
            Debug.Log($"[scene]:Main [load progress]: {asyncLoad.progress}");

            yield return null;
        }
    }

    public void JumpScene(VideoPlayer vp)
    {
        asyncLoad.allowSceneActivation = true;
    }

    public void SkipVideo()
    {
        if(asyncLoad != null)
        {
            asyncLoad.allowSceneActivation = true;
        }
    }
}
