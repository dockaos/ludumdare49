﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FolderZone : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log("Dropped.");
        if(transform.GetComponentInParent<Window>().ReadOnly) return;
        Transform originalFolder = eventData.pointerDrag.GetComponent<File>().OriginalParent;
        if (originalFolder.GetComponentInParent<Window>().ReadOnly)
        {
            // Make a copy of it
            GameObject newFile = Instantiate(eventData.pointerDrag.gameObject, transform);
            newFile.transform.GetComponent<CanvasGroup>().blocksRaycasts = true;
        }
        else
        {
            eventData.pointerDrag.GetComponent<File>().OriginalParent = transform;
        }
        // Are we the license and Private folder?
        if(transform.GetComponentInParent<Window>().Name == "Private")
        {
            Debug.Log("Dropping on private folder ...");
            if( eventData.pointerDrag.GetComponent<File>().Name == "license" )
            {
                Debug.Log("Dropping license");
                OSManager.instance.LicenseInPrivate = true;
            }
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        //Debug.Log("Pointer In.");
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //Debug.Log("Pointer exit.");
    }
}
