﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class FileIcon : MonoBehaviour, IPointerClickHandler
{

    public FileType fileType;

    public void OnPointerClick(PointerEventData eventData)
    {
        fileType = transform.GetComponentInParent<File>().fileType;

        Debug.Log("Clickity");
        if (fileType.name == "Folder")
        {
            Debug.Log("Opening window");
            return;
        }
        if (fileType.name == "Trophy")
        {
            Debug.Log("Trophy time.");
            return;
        }
    }
}
