﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OSManager : MonoBehaviour
{
    public static OSManager instance = null;
    public InputField SearchBox;

    public GameObject WindowPrefab;
    public GameObject WindowPicPrefab;
    public GameObject WindowTextPrefab;
    public GameObject WindowAuthenticatorPrefab;
    public GameObject WindowAuthenticationPrefab;

    public GameObject FilePrefab;
    public Transform CDObject;
    public Vector3 originalCDPosition;

    public Transform WindowsRoot;
    public Text clock;

    public GameObject ErrorWindow;
    public Text ErrorText;
    public Text ErrorTitle;

    public GameObject WinWindow;
    public AudioClip WinnerSound;

    private GameObject RootWindow;
    private GameObject CDWindow;
    public GameObject AuthenticatorWindow;
    public GameObject AuthenticationWindow;

    public List<GameObject> Windows = new List<GameObject>();
    private GameObject MainExecutable;
    private AudioSource audioSource;
    public AudioClip successClip;

    public GameObject volumeSlider;
    public AudioSource musicSource;
    private Canvas canvas;

    public GameObject Menu;

    public List<AudioClip> ErrorComplaints;

    public GameObject BlueScreen;
    public GameObject JibberJabber;
    public GameObject AuthTray;
    private Vector2 nextAnchor = Vector2.zero;

    // Passes
    public bool LicenseInPrivate = false;
    public bool JibberJabberRunning = false;
    public bool Authenticated = false;

    public void Awake()
    {
        if (instance != null && instance != this)
            Destroy(this.gameObject);
        else
        {
            instance = this;
        }

        // Make sure the cursor stays in the window
        Cursor.lockState = CursorLockMode.Confined;
    }

    public void Start()
    {
        audioSource = transform.GetComponent<AudioSource>();
        originalCDPosition = CDObject.position;
        canvas = WindowsRoot.transform.GetComponentInParent<Canvas>();
    }

    public void Update()
    {
        clock.text = System.DateTime.UtcNow.ToString("ddd HH:mm ");
    }

    public void SearchTime()
    {
        string search_text = SearchBox.text;
        Debug.Log("Searching for ..." + search_text);
        ThrowError("NO SEARCH INDEX", $"Attempt to search for {search_text} but, we ain't found shit.");
    }

    public void OpenRootWindow()
    {
        if( RootWindow == null)
        {
            RootWindow = NewWindow("Root Drive", "Folder");
            _ = NewFile("System", "Folder", RootWindow.transform.Find("MainWindow/Scroll View/Viewport/Content/Files").transform);
            _ = NewFile("DONT_LOOK", "Folder", RootWindow.transform.Find("MainWindow/Scroll View/Viewport/Content/Files").transform);
            _ = NewFile("Games", "Folder", RootWindow.transform.Find("MainWindow/Scroll View/Viewport/Content/Files").transform);
            _ = NewFile("Private", "Folder", RootWindow.transform.Find("MainWindow/Scroll View/Viewport/Content/Files").transform);
            _ = NewFile("Downloads", "Folder", RootWindow.transform.Find("MainWindow/Scroll View/Viewport/Content/Files").transform);
            _ = NewFile("Security", "Folder", RootWindow.transform.Find("MainWindow/Scroll View/Viewport/Content/Files").transform);
        } else
        {
            RootWindow.SetActive(true);
            RootWindow.transform.SetAsLastSibling();
        }
    }

    public void OpenCDWindow()
    {
        if( CDWindow == null)
        {
            CDWindow = NewWindow("LD.JAM.49.Winner.H4x0r", "CD");
            CDWindow.GetComponent<Window>().SetReadOnly(true);
            MainExecutable = NewFile("LDJAM49_APP", "Trophy", CDWindow.transform.Find("MainWindow/Scroll View/Viewport/Content/Files").transform);
            _ = NewFile("license", "Text", CDWindow.transform.Find("MainWindow/Scroll View/Viewport/Content/Files").transform);
        } else
        {
            CDWindow.SetActive(true);
            CDWindow.transform.SetAsLastSibling();
        }
        CDObject.GetComponent<Animator>().enabled = false;
        CDObject.position = originalCDPosition;
    }

    public GameObject NewWindow(string name, string type)
    {
        GameObject win = null;
        if (type == "Image")
        {
            win = (GameObject)Instantiate(WindowPicPrefab, WindowsRoot, false);
            PictureWindow window = win.GetComponent<PictureWindow>();
            window.SetPhoto(name);
            window.SetName(name);
            window.SetType(type);
        }
        else if (type == "Text")
        {
            win = (GameObject)Instantiate(WindowTextPrefab, WindowsRoot, false);
            TextWindow window = win.GetComponent<TextWindow>();
            window.SetText(name);
            window.SetName(name);
            window.SetType(type);
        } else if( type == "Authenticator")
        {
            win = (GameObject)Instantiate(WindowAuthenticatorPrefab, WindowsRoot, false);
            AuthenticatorWindow window = win.GetComponent<AuthenticatorWindow>();
            window.SetName(name);
            window.SetType(type);
        }
        else if (type == "Authentication")
        {
            win = (GameObject)Instantiate(WindowAuthenticationPrefab, WindowsRoot, false);
            AuthenticationWindow window = win.GetComponent<AuthenticationWindow>();
            window.SetName(name);
            window.SetType(type);
        } else
        {
            win = (GameObject)Instantiate(WindowPrefab, WindowsRoot, false);
            Window window = win.GetComponent<Window>();
            window.SetName(name);
            window.SetType(type);
            win.GetComponent<RectTransform>().anchoredPosition = nextAnchor;
            nextAnchor += new Vector2(10.0f, -10.0f);
        }
        win.transform.SetAsLastSibling();
        if( Mathf.Abs(nextAnchor.y) * canvas.scaleFactor + 50.0f > Screen.height ||
            Mathf.Abs(nextAnchor.x) * canvas.scaleFactor + 200.0f > Screen.width)
        {
            nextAnchor = Vector2.zero;
        }
        return win;
    }

    public void OpenWindow(string name, string type, bool read_only)
    {
        bool found = false;
        foreach(GameObject win in Windows)
        {
            if(win.GetComponent<Window>().Name == name)
            {
                found = true;
                win.SetActive(true);
                win.transform.SetAsLastSibling();
            } 
        }
        if(!found)
        {
            GameObject NewWin = NewWindow(name, type);
            NewWin.GetComponent<Window>().SetReadOnly(read_only);
            if(name == "Downloads")
            {
                _ = NewFile("JibbaJabba_Torrent", "Radioactive", NewWin.transform.Find("MainWindow/Scroll View/Viewport/Content/Files").transform);
            } else if( name == "DONT_LOOK")
            {
                _ = NewFile("pickaxe", "Image", NewWin.transform.Find("MainWindow/Scroll View/Viewport/Content/Files").transform);
                _ = NewFile("dopey", "Image", NewWin.transform.Find("MainWindow/Scroll View/Viewport/Content/Files").transform);
                _ = NewFile("sleepy", "Image", NewWin.transform.Find("MainWindow/Scroll View/Viewport/Content/Files").transform);
                _ = NewFile("duh", "Image", NewWin.transform.Find("MainWindow/Scroll View/Viewport/Content/Files").transform);
                _ = NewFile("unsure", "Image", NewWin.transform.Find("MainWindow/Scroll View/Viewport/Content/Files").transform);
                _ = NewFile("screamy", "Image", NewWin.transform.Find("MainWindow/Scroll View/Viewport/Content/Files").transform);
            }
            else if (name == "Games")
            {
                _ = NewFile("README", "Text", NewWin.transform.Find("MainWindow/Scroll View/Viewport/Content/Files").transform);
            }
            else if (name == "System")
            {
                _ = NewFile("Jibber Jabber Client", "JibberJabber", NewWin.transform.Find("MainWindow/Scroll View/Viewport/Content/Files").transform);
                _ = NewFile("Search", "Folder", NewWin.transform.Find("MainWindow/Scroll View/Viewport/Content/Files").transform);
            }
            else if (name == "Security")
            {
                _ = NewFile("Secret", "Folder", NewWin.transform.Find("MainWindow/Scroll View/Viewport/Content/Files").transform);
            }
            else if (name == "Secret")
            {
                _ = NewFile("Auth", "Folder", NewWin.transform.Find("MainWindow/Scroll View/Viewport/Content/Files").transform);
            }
            else if (name == "Auth")
            {
                _ = NewFile("Authentication", "Authentication", NewWin.transform.Find("MainWindow/Scroll View/Viewport/Content/Files").transform);
            }
            Windows.Add(NewWin);
        }
    }

    public GameObject NewFile(string name, string type, Transform folder)
    {
        GameObject new_file = (GameObject)Instantiate(FilePrefab, folder, false);
        File file = new_file.GetComponent<File>();
        file.SetName(name);
        file.SetType(type);
        return new_file;
    }

    public void ThrowError(string ErrorType, string ErrorMessage)
    {
        ErrorTitle.text = ErrorType;
        ErrorText.text = ErrorMessage;
        ErrorWindow.SetActive(true);
        ErrorWindow.GetComponent<CanvasGroup>().blocksRaycasts = true;
        audioSource.clip = ErrorComplaints[Random.Range(0, ErrorComplaints.Count)];
        audioSource.Play();
    }

    public void CloseError()
    {
        ErrorWindow.SetActive(false);
        ErrorWindow.GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void ShowWin()
    {
        WinWindow.SetActive(true);
        WinWindow.transform.GetComponent<CanvasGroup>().blocksRaycasts = true;
        audioSource.clip = WinnerSound;
        audioSource.Play();
    }

    public void CheckWin(Transform trophy)
    {
        Dictionary<string, string> Errors = new Dictionary<string, string>();
        // ReadOnly?
        if( trophy.GetComponentInParent<Window>().ReadOnly)
        {
            Errors.Add("READ-ONLY FILE SYSTEM", "Unable to create save file, disk is read only. Are you attempting to run from a CD?");
        }
        if( musicSource.volume > 0.0f )
        {
            Errors.Add("AUDIO SYSTEM IN USE", "Unable to gain exlusive access to the fine audio system on this machine. It is already in use by another program.");
        }
        if( ! LicenseInPrivate )
        {
            Errors.Add("PUBLIC LICENSE!!!", "Licenses must be kept Private! Do you want the whole world checking out your license? No. Of course you don't. Fix that!");
        }
        if (!JibberJabberRunning)
        {
            Errors.Add("JIBBER JABBER CLIENT NOT RUNNING", "Ludum Dare 49 Winning Game Pirate Copy requires the Jibber Jabber client to be running. It doesn't appear to be started, please start it before trying again.");
        }

        if (Errors.Count == 0)
        {
            // They won!
            ShowWin();
        }
        else
        {
            int rando = Random.Range(0, Errors.Count);
            int count = 0;
            foreach (KeyValuePair<string, string> entry in Errors)
            {
                if (count == rando)
                {
                    ThrowError(entry.Key, entry.Value);
                    return;
                }
                count++;
            }
            // How did we get here? It's a real error! :D
            ThrowError("SOMETHING BAD HAPPENED", "There's a real problem with the game. It's just not stable.");
        }

    }

    public void ToggleVolumeSlider()
    {
        volumeSlider.SetActive(!volumeSlider.activeSelf);
    }

    public void SetVolume()
    {
        musicSource.volume = volumeSlider.GetComponent<Slider>().value;
    }

    public void Crash()
    {
        BlueScreen.SetActive(true);
    }

    public void Reset()
    {
        SceneManager.LoadScene("Main");
    }

    public void ToggleMenu()
    {
        Menu.SetActive(!Menu.activeSelf);
    }

    public void ToggleJibberJabber()
    {
        JibberJabber.SetActive(!JibberJabber.activeSelf);
        JibberJabberRunning = JibberJabber.activeSelf;
        audioSource.clip = successClip;
        audioSource.Play();
    }

    public void ToggleAuthenticate()
    {
        AuthTray.SetActive(!AuthTray.activeSelf);
        Authenticated = AuthTray.activeSelf;
        audioSource.clip = successClip;
        audioSource.Play();
    }

    public void ShowAuthenticatorWindow()
    {
        if (AuthenticatorWindow == null)
        {
            AuthenticatorWindow = NewWindow("LDOS Authenticator", "Authenticator");
        }
        else
        {
            AuthenticatorWindow.SetActive(true);
            AuthenticatorWindow.transform.SetAsLastSibling();
        }
        ToggleMenu();
    }

    public void ShowAuthenticationWindow()
    {
        if (AuthenticationWindow == null)
        {
            AuthenticationWindow = NewWindow("Authentication Window", "Authentication");
        }
        else
        {
            AuthenticationWindow.SetActive(true);
            AuthenticationWindow.transform.SetAsLastSibling();
        }
    }

    public void ShowToolTip(string tip)
    {
        ToolTip.ShowToolTip_Static(tip);
    }

    public void HideToolTip()
    {
        ToolTip.HidToolTip_Static();  
    }

}
