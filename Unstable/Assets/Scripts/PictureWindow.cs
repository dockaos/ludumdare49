﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PictureWindow : Window
{
    public Image photo;
    public Sprite currentSprite;

    public void SetPhoto(string image_location)
    {
        currentSprite = Resources.Load<Sprite>("Pics/" + image_location);
        photo.sprite = currentSprite;
        photo.SetNativeSize();
        Debug.Log("Set photo to " + "Pics/" + image_location);
    }
}
