﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[System.Serializable]
public class FolderType
{
    public string name;
    public Sprite sprite;
}


public class Window : MonoBehaviour, IPointerDownHandler
{

    public List<FolderType> FolderTypes;

    public string Name;
    public string type;
    public bool ReadOnly = false;

    public Text Title;
    public Image Type;

    [SerializeField]
    private RectTransform myWindow;

    private Canvas canvas;

    public void Awake()
    {
        canvas = transform.GetComponentInParent<Canvas>();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        myWindow.SetAsLastSibling();
    }

    public void CloseWindow()
    {
        gameObject.SetActive(false);
    }

    public void MaximizeWindow()
    {
        RectTransform rect = transform.GetComponent<RectTransform>();
        RectTransform root = transform.parent.GetComponent<RectTransform>();
        rect.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0.0f, root.rect.width);
        rect.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0.0f, root.rect.height);
        transform.SetAsLastSibling();
    }

    public void SetName(string name)
    {
        Name = name;
        Title.text = Name;
    }

    public void SetType(string type)
    {
        foreach(FolderType ft in FolderTypes)
        {
            if( ft.name == type)
            {
                Type.sprite = ft.sprite;
            }
        }
    }

    public void SetReadOnly(bool read)
    {
        ReadOnly = read;
    }

   

}