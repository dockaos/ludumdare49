﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AuthenticationWindow : Window
{

    public InputField input;

    public void CheckInput()
    {
        if( OSManager.instance.AuthenticatorWindow == null)
        {
            OSManager.instance.ThrowError("WHAT?", "Did you just make up that code? You need to run the Authenticator to see the key.");
            CloseWindow();
            return;
        }
        if( OSManager.instance.AuthenticatorWindow.GetComponent<AuthenticatorWindow>().currentMessage == input.text)
        {
            OSManager.instance.ToggleAuthenticate();
            CloseWindow();
            return;
        }
        OSManager.instance.ThrowError("INVALID AUTHENTICATION CODE", "That code does not match the current authentication value.");
    }

}
