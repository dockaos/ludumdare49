﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextWindow : Window
{
    public Text textArea;

    public void SetText(string text_file)
    {
        TextAsset my_text = (TextAsset)Resources.Load("Text/" + text_file, typeof(TextAsset));
        Debug.Log("MyText: " + my_text.text);
        textArea.text = my_text.text;
    }
}
