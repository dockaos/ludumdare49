﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToolTip : MonoBehaviour
{
    private static ToolTip instance;
    private RectTransform rectTransform;
    private Text text;

    public void Awake()
    {
        instance = this;
        rectTransform = transform.Find("Background").GetComponent<RectTransform>();
        text = transform.GetComponentInChildren<Text>();

        ShowToolTip("Well here's the thing.");
        gameObject.SetActive(false);
    }
    private void ShowToolTip(string tip)
    {
        text.text = tip;
        Vector2 backgroundSize = new Vector2(text.preferredWidth + 8f, text.preferredHeight + 8f);
        rectTransform.sizeDelta = backgroundSize;
        gameObject.SetActive(true);

    }

    private void HideToolTip()
    {
        gameObject.SetActive(false);
    }

    public void Update()
    {
        Vector2 localPoint;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(transform.parent.GetComponent<RectTransform>(),
            Input.mousePosition, null, out localPoint);
        transform.localPosition = localPoint;
    }

    public static void ShowToolTip_Static(string tooltip)
    {
        instance.ShowToolTip(tooltip);
    }

    public static void HidToolTip_Static()
    {
        instance.HideToolTip();
    }
}
