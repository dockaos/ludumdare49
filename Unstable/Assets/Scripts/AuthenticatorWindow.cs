﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AuthenticatorWindow : Window
{
    public Text mainText;

    public List<string> authMessages = new List<string> { "spaghetti", "mike", "kaos", "123456", "hunter2" };

    public string currentMessage = "";

    public float timeToSwitch = 0.0f;
    public float maxTime = 10.0f;

    // Update is called once per frame
    void Update()
    {
        if(timeToSwitch <= 0.0f )
        {
            timeToSwitch = maxTime;
            currentMessage = authMessages[Random.Range(0, authMessages.Count)];
            mainText.text = currentMessage;
        }
        timeToSwitch -= Time.deltaTime;
    }
}
