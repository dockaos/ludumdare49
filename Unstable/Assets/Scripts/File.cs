﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[System.Serializable]
public class FileType
{
    public string name;
    public Sprite sprite;
}

public class File : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler, IPointerClickHandler,
    IPointerEnterHandler, IPointerDownHandler, IPointerUpHandler, ISelectHandler
{
    [SerializeField]
    private RectTransform myFolder;
    [SerializeField]
    public List<FileType> FileTypes;
    public string Name;
    public string type;
    public FileType fileType;

    public Text Title;
    public Image Type;

    private Canvas canvas;
    public Transform OriginalParent = null;

    public void Awake()
    {
        canvas = transform.GetComponentInParent<Canvas>();
        myFolder = transform.GetComponentInParent<RectTransform>();
    }

    public void OnDrag(PointerEventData eventData)
    {
        myFolder.anchoredPosition += eventData.delta / canvas.scaleFactor;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        OriginalParent = transform.parent;
        transform.SetParent(canvas.transform);
        // Stop block Raycast
        transform.GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("Returning to original parent");
        transform.SetParent(OriginalParent);
        transform.GetComponent<CanvasGroup>().blocksRaycasts = true;
    }

    public void SetName(string name)
    {
        Name = name;
        Title.text = Name;
    }

    public void SetType(string type)
    {
        foreach (FileType ft in FileTypes)
        {
            if (ft.name == type)
            {
                Type.sprite = ft.sprite;
                fileType = ft;
            }
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log("Pointer click " + transform.name);
        OpenFile();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        //Debug.Log("Pointer Enter: " + transform.name);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //Debug.Log("Pointer Exit: " + transform.name);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
       
    }

    public void OnPointerUp(PointerEventData eventData)
    {

    }

    public void OnSelect(BaseEventData eventData)
    {
        Debug.Log(" Select: " + transform.name);
    }

    public void OpenFile() { 

        Debug.Log("Clickity");
        if (fileType.name == "Folder")
        {
            Debug.Log("Opening window");
            if( Name == "System" && ! OSManager.instance.Authenticated)
            {
                OSManager.instance.ThrowError("UNAUTHENTICATED!", "Hey! You are trying to access system files, you need to be authenticated. Rules are rules.");
                return;
            }
            OSManager.instance.OpenWindow(Name, "Folder", transform.GetComponentInParent<Window>().ReadOnly);
            return;
        }
        if (fileType.name == "Trophy")
        {
            Debug.Log("Trophy time.");
            OSManager.instance.CheckWin(this.transform);
            return;
        }
        if (fileType.name == "Radioactive")
        {
            OSManager.instance.Crash();
        }
        if (fileType.name == "Image")
        {
            Debug.Log("Opening picture window");
            OSManager.instance.OpenWindow(Name, fileType.name, transform.GetComponentInParent<Window>().ReadOnly);
            return;
        }
        if (fileType.name == "Text")
        {
            Debug.Log("Opening text window");
            OSManager.instance.OpenWindow(Name, fileType.name, transform.GetComponentInParent<Window>().ReadOnly);
            return;
        }
        if (fileType.name == "Authentication")
        {
            Debug.Log("Opening authentication window");
            OSManager.instance.OpenWindow(Name, fileType.name, transform.GetComponentInParent<Window>().ReadOnly);
            return;
        }
        if( fileType.name == "JibberJabber")
        {
            OSManager.instance.ToggleJibberJabber();
        }
    }


}
